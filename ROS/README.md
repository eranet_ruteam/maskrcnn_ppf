## Build ROS and start MaskRCNN with python3 venv ##

1) activate markcnn python env (source /home/era/maskrcnn_ppf/samples/coco/env/bin/activate)
2) install ROS-kinetic-BASE from sources (how-to based on https://www.miguelalonsojr.com/blog/robotics/ros/python3/2019/08/20/ros-melodic-python-3-build.html (adopted for kinetic)):
```sudo -H pip3 install rosdep rospkg rosinstall_generator rosinstall wstool vcstools catkin_tools catkin_pkg
sudo rosdep init
rosdep update```

```cd ~
mkdir ros_catkin_ws
cd ros_catkin_ws```

```catkin config --init -DCMAKE_BUILD_TYPE=Release --blacklist rqt_rviz rviz_plugin_tutorials librviz_tutorial --install```

```rosinstall_generator ros_base --rosdistro kinetic --deps --tar > kinetic-ros-base.rosinstall
wstool init -j8 src kinetic-ros-base.rosinstall
wstool update -j4 -t src```

```export ROS_PYTHON_VERSION=3
pip3 install -U -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-16.04 wxPython```

```catkin build```

3) create workspace where (CATKIN_ROOT =~/maskrcnn_ppf/ROS/catkin_ws; PYENV=/home/era/maskrcnn_ppf/samples/coco/env/bin/python3.6)
```source $PYENV/activate
mkdir -p $CATKIN_ROOT/catkin_ws/src
cd $CATKIN_ROOT/catkin_ws/
catkin_make -DPYTHON_EXECUTABLE=$PYENV/python3.6```

4) create package
```catkin_create_pkg maskrcnn_ppf std_msgs rospy roscpp
catkin_make -DPYTHON_EXECUTABLE=$PYENV/python3.6```


5) start
```cd $CATKIN_ROOT/catkin_ws/
source $PYENV/activate
source ./devel/setup.bash```

```rosrun maskrcnn_ppf maskrcnn_ppf_matching.py --src_img video --mode 6dof```
