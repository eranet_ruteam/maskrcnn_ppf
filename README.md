INSTALLATION

dependences:
NVIDIA-SMI      390.12
Driver Version: 390.12
GPU Version     tensorflow_gpu-1.4.0
Python version  2.7, 3.3-3.6
Compiler        GCC 4.8 or higher
Build tools     Bazel 0.5.4
CUDA            8
cuDNN           6

https://www.geforce.com/drivers
https://www.geforce.com/drivers/results/128743
https://developer.nvidia.com/cuda-80-ga2-download-archive
https://developer.nvidia.com/rdp/cudnn-archive

    1) Clone this repository: https://github.com/matterport/Mask_RCNN
    2) install requirements:
        Package                Version
        ---------------------- ---------
        backcall               0.1.0
        bleach                 1.5.0
        cycler                 0.10.0
        Cython                 0.29.15
        decorator              4.4.1
        enum34                 1.1.6
        h5py                   2.10.0
        html5lib               0.9999999
        imageio                2.6.1
        imgaug                 0.2.9
        ipython                7.12.0
        ipython-genutils       0.2.0
        jedi                   0.16.0
        joblib                 0.14.1
        Keras                  2.0.8
        kiwisolver             1.1.0
        Markdown               3.2
        matplotlib             3.1.3
        networkx               2.4
        numpy                  1.15.0
        opencv-python          4.2.0.32
        parso                  0.6.1
        pexpect                4.8.0
        pickleshare            0.7.5
        Pillow                 7.0.0
        pip                    20.0.2
        prompt-toolkit         3.0.3
        protobuf               3.11.3
        ptyprocess             0.6.0
        pycocotools            2.0.0
        Pygments               2.5.2
        pyparsing              2.4.6
        python-dateutil        2.8.1
        PyWavelets             1.1.1
        PyYAML                 5.3
        scikit-image           0.16.2
        scikit-learn           0.22.1
        scipy                  1.4.1
        setuptools             45.2.0
        Shapely                1.7.0
        six                    1.14.0
        sklearn                0.0
        tensorflow-gpu         1.4.0
        tensorflow-tensorboard 0.4.0
        traitlets              4.3.3
        wcwidth                0.1.8
        Werkzeug               1.0.0
        wheel                  0.34.2
    3) Run setup from the repository root directory: python3 setup.py install
    4) Install pycocotools from repository https://github.com/waleedka/coco
         clone https://github.com/waleedka/coco.git
         cd ./coco/PythonAPI
         make or for install pycocotools locally: python setup.py build_ext --inplace of for install pycocotools to the Python site-packages: python setup.py build_ext install


COMMAND TO TRAIN MASKRCNN:

    1) Train a new model starting from pre-trained COCO weights:
        python3 ~/samples/coco/coco.py train --dataset=/path/to/cocodataset --model=coco
    2) Continue training a model that you had trained earlier:
        python3 ~/samples/coco/coco.py train --dataset=/path/to/cocodataset --model=/path/to/weights.h5
    3) Continue training the last model you trained. This will find the last trained weights in the model directory:
        python3 ~/samples/coco/coco.py train --dataset=/path/to/cocodataset --model=last
        
DATA:
    
    CAD models of objects have to be with null at the centroid.
    You can download weights for semantic segmentation of the following objects: 'OBJ2', 'OBJ3', 'OBJ4' from: https://drive.google.com/open?id=10E-lpwX_qpOGZQrIbwo9iw-aWxNP4_xb

COMMAND TO RUN POSE ESTIMATION:
    
    1) Visualize the results of semantic segmentation on video: 
    python3 ~/samples/pose_estimation/src/maskrcnn_ppf_matching.py --src_img video --mode seg
    2) Visualize the results of semantic segmentation from camera:
    python3 ~/samples/pose_estimation/src/maskrcnn_ppf_matching.py --src_img cam --mode seg
    3) Pose estimation on video:
    python3 ~/samples/pose_estimation/src/maskrcnn_ppf_matching.py --src_img video --mode 6dof
    4) Pose estimation on camera:
    python3 ~/samples/pose_estimation/src/maskrcnn_ppf_matching.py --src_img cam --mode 6dof


COCO format for annotation: 

    {"categories": [{"id": int, "name": str, "color": str}],
                   "images": [{"id": int, "width": int, "height": int, "file_name": str, "path": str}],
                   "annotations": [{"id": int, "image_id": int, "category_id": int, "width": int, "height": int,
                                   "area": int, "segmentation": [], "bbox": [], "color": str, "iscrowd": int}]}

CHECK ALL PATHS:

    1) coco.py - script for training and configure model:
             coco - path to annotation file, check signature of name with your data (*.json)
             image_dir - path to directories with subsets of images for "train", "val" and "test"
             if you want to start your experiment with your weights use command line with key --model=/path/to/your/weights
    2) evaluation.py - script to evaluate your weights and get metrics mAP and IoU:
             subset - subset "val" or "train" or "test" for choosing right .json annotation file
             dataset - path to images same that mentioned in annotation file
             model_path - path to your weights
    3) inference.py - script that include function "get_image_predict"
             weights_path - path to your weights
    4) model_using.py - script that shows how your trained model works with real images:
             model_path - path to your weights
             write your way to directory with .png in the cycle
    5) visualizations.py - script that shows how model detect objects with using annotation file:
             COCO_DIR - directory with images
             dataset.load_coco(COCO_DIR, "val") write here subset for that annotation file you want to use
    6) maskrcnn_ppf_matching.py
             sys.path.append("/home/era/Mask_RCNN/samples/coco")  write here your local path to directory with coco.py
             CAD_PATH - path to your CAD data
             cap_RGB - path to *RGB.avi'
             cap_Depth - path to *Depth.avi')
             pinhole_camera_intrinsic - path to annotation .json


    