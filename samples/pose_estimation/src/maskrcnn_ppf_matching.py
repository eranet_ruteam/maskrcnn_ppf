import os
import sys
import time
import numpy as np
import cv2
import scipy.ndimage as ndimage
import matplotlib.pyplot as pyplot
import open3d as o3d
from scipy.ndimage.filters import gaussian_filter
import argparse

#Import function for inference of MaskRCNN
sys.path.append("/home/era/maskrcnn_ppf/samples/coco")  
from inference import get_image_predict

#For visualization result of semantic segmentation
def color_map(N=256, normalized=True):
    def bitget(byteval, idx):
        return ((byteval & (1 << idx)) != 0)

    dtype = 'float32' if normalized else 'uint8'
    cmap = np.zeros((N, 3), dtype=dtype)
    for i in range(N):
        r = g = b = 0
        c = i
        for j in range(8):
            r = r | (bitget(c, 0) << 7 - j)
            g = g | (bitget(c, 1) << 7 - j)
            b = b | (bitget(c, 2) << 7 - j)
            c = c >> 3

        cmap[i] = np.array([r, g, b])

    cmap = cmap / 255 if normalized else cmap
    return cmap

#Get intrinsic matrix of IntelRealSense
def get_intrinsic_matrix(frame):
    intrinsics = frame.profile.as_video_stream_profile().intrinsics
    out = o3d.camera.PinholeCameraIntrinsic(640, 480, intrinsics.fx,
                                            intrinsics.fy, intrinsics.ppx,
                                            intrinsics.ppy)
    return out

#ICU_data
CAD_PATH = '/home/era/maskrcnn_ppf/samples/pose_estimation/data/icu/CAD'

class_names = ['BG', 'OBJ2', 'OBJ3', 'OBJ4',]

parser = argparse.ArgumentParser(description='test.')
parser.add_argument('--src_img', required=True,
                    metavar="<'cam'|'video'>",
                    help="choose the source of images 'cam' or 'video'") 
parser.add_argument('--mode', required=True,
                    metavar="<'seg'|'6dof'>",
                    help="choose mode 'seg' or '6dof'") 

args = parser.parse_args()

#Computing initial model for 3D_matching
cad_models = os.listdir(CAD_PATH)
lst_detector_md = {}
lst_normals_md = {}
for model in cad_models:
    cad_model_path = os.path.join(CAD_PATH, model)
    pcModel = cv2.ppf_match_3d.loadPLYSimple(cad_model_path, 1)
    tt, normals_md = cv2.ppf_match_3d.computeNormalsPC3d(pcModel, NumNeighbors=6, FlipViewpoint=0, viewpoint=(0, 0, 0))

    detector = cv2.ppf_match_3d_PPF3DDetector(0.025, 0.05)
    print('Training...')
    detector.trainModel(normals_md)
    print("Trained")
    obj_name = model.split('.')
    lst_detector_md.update({obj_name[0]: detector})
    lst_normals_md.update({obj_name[0]: normals_md})

#[Source_image] Video: RGB&Depth
if args.src_img == "video":
    cap_RGB = cv2.VideoCapture('/home/era/pose_estimation/data/icu/Pose_1_RGB.avi')
    cap_Depth = cv2.VideoCapture('/home/era/pose_estimation/data/icu/Pose_1_Depth.avi')
    succ = cap_RGB.isOpened() and cap_Depth.isOpened()
 
#[Source_image] IntelRealSence
if args.src_img == "cam":
    import pyrealsense2 as rs
    pipeline = rs.pipeline()

    #Create a config and configure the pipeline to stream
    #  different resolutions of color and depth streams
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    # Start streaming
    profile = pipeline.start(config)
    align_to = rs.stream.color
    align = rs.align(align_to)
    succ = True

cmap = color_map()

# Read until video is completed
while succ:
    if args.src_img == "video":
        ret_rgb, source_color = cap_RGB.read()
        ret_depth, source_depth = cap_RGB.read()
        image = source_color

    if args.src_img == "cam":

        frames = pipeline.wait_for_frames()
        aligned_frames = align.process(frames)

        aligned_depth_frame = aligned_frames.get_depth_frame()  # aligned_depth_frame is a 640x480 depth image
        aligned_color_frame = aligned_frames.get_color_frame()
        image = aligned_color_frame

        source_depth = o3d.geometry.Image(np.array(aligned_depth_frame.get_data()))
        source_color = o3d.geometry.Image(np.asarray(aligned_color_frame.get_data()))

    #Results of inference MaskRCNN
    res = get_image_predict(image) 
    r = res[0]
    masks = r['masks']

    num_instances = r['rois'].shape[0]
    show_img = image.copy()

    #Mode - 2D semantic segmentation with visualization
    if args.mode == 'seg':
        # Draw all masks
        for idx in range(num_instances):
            mask = r['masks'][:, :, idx]
            color = cmap[idx+1]
            class_id = r['class_ids'][idx]
            mask_pixs = np.argwhere(mask)[:, ::-1]

            center = np.mean(mask_pixs, axis=0).astype(int)
            alpha = 0.5
            for c in range(3):

                show_img[:, :, c] = np.where(mask == 1,
                                             show_img[:, :, c] *
                                             (1 - alpha) + alpha * color[c] * 255,
                                             show_img[:, :, c])
            cv2.putText(show_img, class_names[class_id], tuple(center), cv2.LINE_AA, 0.5, (255, 255, 255), 2)
        show_img = cv2.cvtColor(show_img, cv2.COLOR_RGB2BGR)
        cv2.imshow('res', show_img)
        cv2.waitKey(100)
        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

    #Mode - 6DoF
    if args.mode == '6dof':
        common_pc = []
        rt_results = {}
        for idx in range(num_instances):
            #Get mask of the object on the image
            mask = r['masks'][:, :, idx]
            class_id = r['class_ids'][idx]
            obj_name = class_names[class_id]

            mask_pixs = np.argwhere(mask)[:, ::-1]

            edges = (ndimage.filters.maximum_filter(mask, size=2) == ndimage.filters.minimum_filter(mask, size=2))
            points = []
            for i in range(0, 479):
                for j in range(0, 639):
                    if not (edges[i, j]):
                       points.append([i, j])

            a3 = np.array(points, dtype=np.int32)
            width, height, vec = np.asarray(source_color).shape
            im = np.zeros([width, height], dtype=np.uint8)
            cv2.fillPoly(im, [a3], 255)

            im = gaussian_filter(im, sigma=6)
            _, mask_im = cv2.threshold(im, thresh=2, maxval=255, type=cv2.THRESH_BINARY)

            masked_data_RGB = np.asarray(cv2.bitwise_and(np.asarray(source_color), np.asarray(source_color), mask=mask_im))
            masked_data_depth = np.asarray(cv2.bitwise_and(np.asarray(source_depth), np.asarray(source_depth), mask=mask_im))
            masked_data_depth_c = np.copy(masked_data_depth)
            masked_data_depth_c = masked_data_depth_c[masked_data_depth_c != 0]
            mval = np.mean(masked_data_depth_c)

            #Create point cloud from RGB&Depth frames
            masked_data_depth[masked_data_depth > mval] = 0
            img = o3d.geometry.Image(masked_data_RGB.astype(np.uint8))
            dpt = o3d.geometry.Image(masked_data_depth.astype(np.uint16))

            source_rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(
                img, dpt)

            pinhole_camera_intrinsic = o3d.io.read_pinhole_camera_intrinsic('/home/era/pose_estimation/data/icu/intrinsic.json')

            scene_pcd_color_term = o3d.geometry.PointCloud.create_from_rgbd_image(
                source_rgbd_image, pinhole_camera_intrinsic)
            scene_pcd_color_term.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
            o3d.visualization.draw_geometries([scene_pcd_color_term])
            # cv2.ppf_match_3d.writePLY(scene_pcd_color_term, "bolt_normals.ply")

            #3D matching
            pcTest = np.asarray(scene_pcd_color_term.points, np.float32)
            pp, normals_sc = cv2.ppf_match_3d.computeNormalsPC3d(pcTest, NumNeighbors=6, FlipViewpoint=0, viewpoint=(0, 0, 0))
            detector = lst_detector_md[obj_name]
            normals_md = lst_normals_md[obj_name]

            print('Matching...')
            results = detector.match(normals_sc, 1.0/40.0, 0.05)
            print('Performing ICP...')
            icp = cv2.ppf_match_3d_ICP(100)
            _, results = icp.registerModelToScene(normals_md, normals_sc, results[:2])
            print("Poses: ")
            for i, result in enumerate(results):
                print("\n-- Pose to Model Index %d: NumVotes = %d, Residual = %f\n%s\n" % (result.modelIndex, result.numVotes, result.residual, result.pose))
                if i == 0:
                    pct = cv2.ppf_match_3d.transformPCPose(normals_md, result.pose)
                    rt_results.update({obj_name: result.pose})
                    cv2.ppf_match_3d.writePLY(pct, obj_name + "_PCTrans.ply")
            print(np.asarray(pct[:, :3]))

        
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector(np.asarray(pct[:, :3]))
            common_pc.append(pcd)
            common_pc.append(scene_pcd_color_term)
            o3d.visualization.draw_geometries(common_pc)

if args.src_img == "video":
    cv2.destroyAllWindows()

if args.src_img == "cam":
    pipeline.stop()

