import os
import sys
import time
import numpy as np
import cv2

#Import function for inference of MaskRCNN
sys.path.append("/home/era/Mask_RCNN/samples/coco")  
from inference import get_image_predict

#For visualization result of semantic segmentation
def color_map(N=256, normalized=True):
    def bitget(byteval, idx):
        return ((byteval & (1 << idx)) != 0)

    dtype = 'float32' if normalized else 'uint8'
    cmap = np.zeros((N, 3), dtype=dtype)
    for i in range(N):
        r = g = b = 0
        c = i
        for j in range(8):
            r = r | (bitget(c, 0) << 7 - j)
            g = g | (bitget(c, 1) << 7 - j)
            b = b | (bitget(c, 2) << 7 - j)
            c = c >> 3

        cmap[i] = np.array([r, g, b])

    cmap = cmap / 255 if normalized else cmap
    return cmap

#From folder with images
PATH_IMGS = '/home/era/autoannotation/data/nstu/rgb'# ВОТ ЗДЕСЬ ПРОПИСАТЬ ПУТЬ К ТЕСТАМ
imgs = os.listdir(PATH_IMGS)

class_names = ['BG', 'OBJ2', 'OBJ3', 'OBJ4',]
cmap = color_map()
for img in imgs:
    image = cv2.imread(PATH_IMGS+'/'+img)
  
    #Results of inference MaskRCNN
    res = get_image_predict(image) 
    r = res[0]
    masks = r['masks']

    num_instances = r['rois'].shape[0]
    show_img = image.copy()

    #Mode - 2D semantic segmentation with visualization

    for idx in range(num_instances):
        mask = r['masks'][:, :, idx]
        color = cmap[idx+1]
        class_id = r['class_ids'][idx]
        mask_pixs = np.argwhere(mask)[:, ::-1]

        center = np.mean(mask_pixs, axis=0).astype(int)
        alpha = 0.5
        for c in range(3):

            show_img[:, :, c] = np.where(mask == 1,
                                         show_img[:, :, c] *
                                         (1 - alpha) + alpha * color[c] * 255,
                                         show_img[:, :, c])
        cv2.putText(show_img, class_names[class_id], tuple(center), cv2.LINE_AA, 0.5, (255, 255, 255), 2)
    show_img = cv2.cvtColor(show_img, cv2.COLOR_RGB2BGR)
    cv2.imshow('res', show_img)
    cv2.waitKey(100)

    # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
