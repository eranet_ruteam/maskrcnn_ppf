import cv2 as cv
import numpy as np
import open3d as o3d
from scipy.ndimage.filters import gaussian_filter
import json
import copy

def main():
    N = 3
    modelname = "/home/era/pose_estimation/data/nstu/bolt_sample.ply"
    polynape = "/home/era/pose_estimation/data/nstu/polygon.json"
    rgbimage = "/home/era/pose_estimation/data/nstu/rgb.png"
    dimage = "/home/era/pose_estimation/data/nstu/depth.png"
    intrinsic_file ="/home/era/pose_estimation/data/nstu/intrinsic.json"
    pc_scene = "/home/era/pose_estimation/data/nstu/scene_pc.ply"

    # MODEL
    pcModel = cv.ppf_match_3d.loadPLYSimple(modelname, 1)
    tt, normals_md = cv.ppf_match_3d.computeNormalsPC3d(pcModel, NumNeighbors=6, FlipViewpoint=0, viewpoint=(0, 0, 0))
    detector = cv.ppf_match_3d_PPF3DDetector(0.025, 0.05)
    # TRAINING
    print('Training...')

    detector.trainModel(normals_md)

    print("Trained")

    #READ RGB AND DEPTH
    source_color = o3d.io.read_image(rgbimage)
    source_depth = o3d.io.read_image(dimage)

    with open(polynape) as f:
        parser = json.load(f)
    points = [parser['polygon']]

    a3 = np.array(points, dtype=np.int32)
    width, height, vec = np.asarray(source_color).shape
    im = np.zeros([width, height], dtype=np.uint8)
    cv.fillPoly(im, a3, 255)

    # GAUSSIAN
    im = gaussian_filter(im, sigma=6)
    _, mask_im = cv.threshold(im, thresh=2, maxval=255, type=cv.THRESH_BINARY)

    # CROPPED BY MASK
    masked_data_RGB = np.asarray(cv.bitwise_and(np.asarray(source_color), np.asarray(source_color), mask=mask_im))
    masked_data_depth = np.asarray(cv.bitwise_and(np.asarray(source_depth), np.asarray(source_depth), mask=mask_im))
    masked_data_depth_c = np.copy(masked_data_depth)
    masked_data_depth_c = masked_data_depth_c[masked_data_depth_c != 0] #DELETE NULL
    mval = np.mean(masked_data_depth_c) #COMPUTE MEAN DEPTH DISTANCE
    masked_data_depth[masked_data_depth > mval] = 0 #FILTER NOISE FROM BACKGROUND
    img = o3d.geometry.Image(masked_data_RGB.astype(np.uint8))
    dpt = o3d.geometry.Image(masked_data_depth.astype(np.uint16))

    #POINT CLOUD
    source_rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(
        img, dpt)
    pinhole_camera_intrinsic = o3d.io.read_pinhole_camera_intrinsic(intrinsic_file)
    scene_pcd_color_term = o3d.geometry.PointCloud.create_from_rgbd_image(
        source_rgbd_image, pinhole_camera_intrinsic)
    scene_pcd_color_term.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
    o3d.visualization.draw_geometries([scene_pcd_color_term])

    #SCENE
    pcTest = np.asarray(scene_pcd_color_term.points, np.float32)
    pp, normals_sc = cv.ppf_match_3d.computeNormalsPC3d(pcTest, NumNeighbors=6, FlipViewpoint=0, viewpoint=(0, 0, 0))

    print('Matching...')
    results = detector.match(normals_sc, 1.0/40.0, 0.05)
    print('Performing ICP...')
    icp = cv.ppf_match_3d_ICP(100)
    _, results = icp.registerModelToScene(normals_md, normals_sc, results[:N])
    print("Poses: ")
    for i, result in enumerate(results):
        print("\n-- Pose to Model Index %d: NumVotes = %d, Residual = %f\n%s\n" % (result.modelIndex, result.numVotes, result.residual, result.pose))
        if i == 0:
            rt_cam = result.pose
            # rt_obj = np.insert(result.pose[:4,:3], [3], [[0],[0],[0],[1]], axis=1)
            pct_cam = cv.ppf_match_3d.transformPCPose(normals_md, rt_cam)
            # pct_obj = cv.ppf_match_3d.transformPCPose(normals_md, rt_obj)
            cv.ppf_match_3d.writePLY(pct_cam, "/home/era/maskrcnn_ppf/samples/pose_estimation/data/nstu/boltPC_camTrans.ply")
            # cv.ppf_match_3d.writePLY(pct_obj, "/home/era/maskrcnn_ppf/samples/pose_estimation/data/nstu/boltPC_objTrans.ply")
    coords = np.asarray(pct_cam[:, :3])
    

    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(np.asarray(pct_cam[:, :3]))
    pcd_scene = o3d.io.read_point_cloud(pc_scene)

    #OBJECT IN THE CROPPED SCENE
    o3d.visualization.draw_geometries([scene_pcd_color_term, pcd])

    #OBJECT IN THE WHOLE SCENE
    o3d.visualization.draw_geometries([pcd_scene, pcd])

if __name__ == "__main__":
    main() 