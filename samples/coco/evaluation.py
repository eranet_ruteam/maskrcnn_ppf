import os
import sys
import numpy as np
from sklearn.metrics import jaccard_score

# Root directory of the project
ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils
import mrcnn.model as modellib
from samples.coco.coco import *


# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")


class InferenceConfig(CocoConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


inference_config = InferenceConfig()

# Recreate the model in inference mode
model = modellib.MaskRCNN(mode="inference", config=inference_config, model_dir=MODEL_DIR)

# Validation dataset
dataset_val = CocoDataset()
subset = "val"
dataset = "/home/era/coco_old/combine_data"

coco = dataset_val.load_coco(dataset, subset, return_coco=True)
dataset_val.prepare()

# Get path to saved weights
# Either set a specific path or find last trained weights
# model_path = model.find_last()
model_path = "/home/era/Mask_RCNN/logs/train_on_combine_data/mask_rcnn_coco_0059.h5"


# Load trained weights
print("Loading weights from ", model_path)
model.load_weights(model_path, by_name=True)

image_ids = dataset_val.image_ids

APs = []
IoU = []

for image_id in image_ids:
    # Load image and ground truth data
    image, image_meta, gt_class_id, gt_bbox, gt_mask = modellib.load_image_gt(dataset_val, inference_config, image_id, use_mini_mask=False)
    molded_images = np.expand_dims(modellib.mold_image(image, inference_config), 0)
    # Run object detection
    results = model.detect([image], verbose=0)
    r = results[0]
    # Compute VOC-Style mAP @ IoU=0.5
    AP, precisions, recalls, overlaps = utils.compute_ap(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"], r["scores"], r['masks'])
    APs.append(AP)

    pred_mask = r["masks"]

    class_ids = r["class_ids"].tolist()
    gt_class_id = gt_class_id.tolist()
    for ids in class_ids:
        pred_mask_new = pred_mask[:, :, class_ids.index(ids)].reshape(1024, 1024)
        pred_mask_new = pred_mask_new.astype(int)
        np.where(pred_mask_new != 0, ids, pred_mask_new)
        if ids in gt_class_id:
            gt_mask_new = gt_mask[:, :, gt_class_id.index(ids)].reshape(1024, 1024)
            gt_mask_new = gt_mask_new.astype(int)
            np.where(gt_mask_new != 0, ids, gt_mask_new)

            # Compute IoU
            metric_iou = jaccard_score(gt_mask_new, pred_mask_new, average='micro')
            IoU.append(metric_iou)


print("mAP: ", np.mean(APs))
print("IoU: ", np.mean(IoU))






