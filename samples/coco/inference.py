import os
import sys
import random
import math
import re
import time
import numpy as np
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Root directory of the project
# ROOT_DIR = os.path.abspath("../../../")
ROOT_DIR = os.path.abspath("/home/era/maskrcnn_ppf")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
print(ROOT_DIR)
from mrcnn import utils
from mrcnn import visualize
from mrcnn.visualize import display_images
import mrcnn.model as modellib
from mrcnn.model import log

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

DEVICE = "/gpu:0"  # /cpu:0 or /gpu:0

# MS COCO Dataset
import coco
config = coco.CocoConfig()


class InferenceConfig(config.__class__):
    # Run detection on one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


config = InferenceConfig()
config.display()

# Create model in inference mode
with tf.device(DEVICE):
    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Set weights file path / load the last model you trained
# uncoment next line to use last trained model
# weights_path = model.find_last()
weights_path = '/home/era/Mask_RCNN/samples/pose_estimation/data/icu/mask_rcnn_icu_0060.h5'

# Load weights
print("Loading weights ", weights_path)
model.load_weights(weights_path, by_name=True)


def get_image_predict(image):
    # Run object detection
    results = model.detect([image[:, :, :3]], verbose=1)

    return results


