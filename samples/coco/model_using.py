from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
import os
import sys
import glob
import time
ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)
from mrcnn.visualize import display_instances
from mrcnn.config import Config
from mrcnn.model import MaskRCNN

# define all classes that the coco model knowns about
class_names = ['BG', "Bolt", "Skoba", "Tarlep"]


# define the test configuration
class TestConfig(Config):
    NAME = "test"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 3


# define the model
rcnn = MaskRCNN(mode='inference', model_dir='./', config=TestConfig())
# load coco model weights
model_path = "/home/era/Mask_RCNN/logs/train_on_combine_data/mask_rcnn_coco_0059.h5"
# load weights
rcnn.load_weights(model_path, by_name=True)

# load photograph
for image in glob.glob("/home/era/autoannotation/data/nstu/rgb/*.png"):
    img = load_img(image)
    img = img_to_array(img)
    # make prediction
    results = rcnn.detect([img], verbose=0)
    # get dictionary for first prediction
    r = results[0]
    # show photo with bounding boxes, masks, class labels and scores
    display_instances(img, r['rois'], r['masks'], r['class_ids'], class_names, r['scores'])

